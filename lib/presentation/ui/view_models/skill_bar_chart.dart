import 'package:dice_stats/data/models/point.dart';
import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class SkillBarChartWidget extends StatefulWidget {

  final Skill skill;
  final Player player;
  final List<Roll> rolls;

  const SkillBarChartWidget(this.skill, this.player, this.rolls, {Key? key})
      : super(key: key);

  @override
  State<SkillBarChartWidget> createState() => _SkillBarChartWidgetState();
}

class _SkillBarChartWidgetState extends State<SkillBarChartWidget> {

  List<Point> barChartPoints = [];

  @override
  void initState() {

    List<int> total = [];

    // Add all roll of the player and sort it
    for (var roll in widget.rolls) {
      total.add(roll.result);
    }
    total.sort();

    // Create a map with the result as key and the count of them as value
    while (total.isNotEmpty) {
      var x = total.first;
      var count = total.where((e) => e == x).length;
      barChartPoints.add(Point(x: x.toDouble(), y: count.toDouble()));
      total.removeWhere((e) => e == x);
    }

    setState(() {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(barChartPoints.isEmpty) {
      return const CircularProgressIndicator();
    }
    return Container(
      margin: const EdgeInsets.only(left: 10, top: 10, right: 50, bottom: 10),
      child: AspectRatio(
        aspectRatio: 2,
        child: BarChart(
            BarChartData(
              barGroups: barChartPoints.map((point) =>
                  BarChartGroupData(
                      x: point.x.toInt(),
                      barRods: [
                        BarChartRodData(
                          toY: point.y,
                          color: Color(widget.player.color),
                        )
                      ]
                  )

              ).toList(),
              borderData: FlBorderData(
                  border: const Border(bottom: BorderSide(), left: BorderSide())),
              gridData: const FlGridData(show: false),
              titlesData: FlTitlesData(
                bottomTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 20)),
                leftTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 40)),
                topTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
                rightTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
              ),
            )
        ),
      ),
    );
  }

  Widget customTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    if (value - 0.5 == value.toInt()) {
      return const Text('');
    }

    final intValue = value.toInt();

    return Padding(
      padding: const EdgeInsets.only(right: 6),
      child: Text(
        intValue.toString(),
        style: style,
        textAlign: TextAlign.center,
      ),
    );
  }

}