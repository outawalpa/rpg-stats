import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class SectionPieChartWidget extends StatefulWidget {

  final Section section;
  final List<Skill> skills;
  final List<Roll> rolls;

  const SectionPieChartWidget(this.section, this.skills, this.rolls, {Key? key})
      : super(key: key);

  @override
  State<SectionPieChartWidget> createState() => _SectionPieChartWidgetState();
}

class _SectionPieChartWidgetState extends State<SectionPieChartWidget> {

  List<PieChartSectionData> pieChartSections = [];

  List<Color> colors = const [
    Color(4294901760),
    Color(4278255360),
    Color(4278190335),
    Color(4294902015),
    Color(4294967040),
    Color(4278255615),
  ];

  @override
  void initState() {

    Map<int, int> count = {};

    for (var roll in widget.rolls) {
      if (count[roll.skillId] == null) {count[roll.skillId] = 0;}
      count[roll.skillId] = count[roll.skillId]! + 1;
    }

    var index = 0;
    count.forEach((key, value) {
      pieChartSections.add(
          PieChartSectionData(
            color: colors[index],
            value: value.toDouble(),
            radius: 80.0,
            title: value.toString(),
            titleStyle: const TextStyle(
              color: Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.bold
            ),
            titlePositionPercentageOffset: 0.8
          )
      );
      index++;
    });

    setState(() {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(pieChartSections.isEmpty) {
      return const CircularProgressIndicator();
    }
    return Container(
      margin: const EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
      child: Column(
        children: [
          AspectRatio(
              aspectRatio: 2,
              child: PieChart(
                  PieChartData(
                      sections: pieChartSections,
                      centerSpaceRadius: 0.0
                  )
              )
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: widget.skills.length/3 % 3 == 0 ? widget.skills.length~/3 : (widget.skills.length/3).truncate() + 1,
            itemBuilder: (context, index) {
              return Row(
                mainAxisAlignment:MainAxisAlignment.spaceBetween,
                children: [
                  if (widget.skills.length -1 >= (index * 3 + 0)) Flexible(
                      flex: 1,
                      child: Row(
                        children: [
                          Icon(Icons.circle, color: colors[(index * 3) + 0]),
                          Text(widget.skills[(index * 3) + 0].title)
                        ],
                      )
                  ),
                  if (widget.skills.length -1 >= (index * 3 + 1)) Flexible(
                      child: Row(
                        children: [
                          Icon(Icons.circle, color: colors[(index * 3) + 1]),
                          Text(widget.skills[(index * 3) + 1].title)
                        ],
                      )
                  ),
                  if (widget.skills.length -1 >= (index * 3 + 2)) Flexible(
                      child: Row(
                        children: [
                          Icon(Icons.circle, color: colors[(index * 3) + 2]),
                          Text(widget.skills[(index * 3) + 2].title)
                        ],
                      )
                  )
                ],
              );
            }
          )
        ],
      )
    );
  }

  Widget customTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    if (value - 0.5 == value.toInt()) {
      return const Text('');
    }

    final intValue = value.toInt();

    return Padding(
      padding: const EdgeInsets.only(right: 6),
      child: Text(
        intValue.toString(),
        style: style,
        textAlign: TextAlign.center,
      ),
    );
  }

}