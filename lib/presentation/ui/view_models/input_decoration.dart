import 'package:flutter/material.dart';

class CustomInputDecoration extends InputDecoration{

  const CustomInputDecoration({required this.hint});
  final String hint;

  @override
  Color? get fillColor => Colors.white;

  @override
  bool? get filled => true;

  @override
  InputBorder? get border => const OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.blueAccent,
      )
  );

  @override
  Color? get focusColor => Colors.white;

  @override
  InputBorder? get focusedBorder => const OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.blueAccent,
      )
  );

  @override
  String? get hintText => hint;

}