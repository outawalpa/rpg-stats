import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class SkillBarChartSeparatedWidget extends StatefulWidget {

  final Skill skill;
  final List<Player> players;
  final Map<int, List<Roll>> rolls;

  const SkillBarChartSeparatedWidget(this.skill, this.players, this.rolls, {Key? key})
      : super(key: key);

  @override
  State<SkillBarChartSeparatedWidget> createState() => _SkillBarChartSeparatedWidgetState();
}

class _SkillBarChartSeparatedWidgetState extends State<SkillBarChartSeparatedWidget> {

  Map<int, List<dynamic>> barChartPoints = {};
  List<BarChartGroupData> barChartGroupData = [];

  @override
  void initState() {

    List<int> total = [];
    for(var player in widget.players) {

      for (var roll in widget.rolls[player.id]!) {
        total.add(roll.result);
      }

      total.sort();

      while (total.isNotEmpty) {
        var x = total.first;
        var count = total.where((e) => e == x).length;
        if(!barChartPoints.containsKey(x)) { barChartPoints[x] = []; }
        barChartPoints[x]?.add([player.color, count.toDouble()]);
        total.removeWhere((e) => e == x);
      }

      var sortedByKeyMap = Map.fromEntries(
          barChartPoints.entries.toList()..sort((e1, e2) => e1.key.compareTo(e2.key)));

      barChartPoints = sortedByKeyMap;

    }

    for (var key in barChartPoints.keys) {
      List<BarChartRodData> barRods = [];
      for(var value in barChartPoints[key]!) {
        barRods.add(BarChartRodData(
          toY: value[1],
          color: Color(value[0]),
        ));
      }
      barChartGroupData.add(BarChartGroupData(
          x: key,
          barRods: barRods
      ));
    }

    setState(() {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(barChartGroupData.isEmpty) {
      return const CircularProgressIndicator();
    }
    return Container(
      margin: const EdgeInsets.only(left: 10, top: 10, right: 50, bottom: 10),
      child: AspectRatio(
        aspectRatio: 2,
        child: BarChart(
            BarChartData(
              barGroups: barChartGroupData,
              borderData: FlBorderData(
                  border: const Border(bottom: BorderSide(), left: BorderSide())),
              gridData: const FlGridData(show: false),
              titlesData: FlTitlesData(
                bottomTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 20)),
                leftTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 40)),
                topTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
                rightTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
              ),
            )
        ),
      ),
    );
  }

  Widget customTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    if (value - 0.5 == value.toInt()) {
      return const Text('');
    }

    final intValue = value.toInt();

    return Padding(
      padding: const EdgeInsets.only(right: 6),
      child: Text(
        intValue.toString(),
        style: style,
        textAlign: TextAlign.center,
      ),
    );
  }
}