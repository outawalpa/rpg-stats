import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class SkillBarChartJoinedWidget extends StatefulWidget {

  final Skill skill;
  final List<Player> players;
  final List<Roll> rolls;

  const SkillBarChartJoinedWidget(this.skill, this.players, this.rolls, {Key? key})
      : super(key: key);

  @override
  State<SkillBarChartJoinedWidget> createState() => _SkillBarChartJoinedWidgetState();
}

class _SkillBarChartJoinedWidgetState extends State<SkillBarChartJoinedWidget> {

  List<BarChartGroupData> barChartGroupData = [];

  @override
  void initState() {

    // Add all roll and sort it
    List<int> total = [];
    for (var roll in widget.rolls) {
      total.add(roll.result);
    }
    total.sort();

    // Create a map with the result as key and the count of them as value
    Map<int, double> barChartPoints = {};
    while (total.isNotEmpty) {
      var x = total.first;
      var count = total.where((e) => e == x).length;
      barChartPoints[x] = count.toDouble();
      total.removeWhere((e) => e == x);
    }

    // Create a list of all player's color
    List<Color> colors = [];
    for (var player in widget.players) {
      colors.add(Color(player.color));
    }

    // Create Data for BarChart with all result and the count of them
    for (var key in barChartPoints.keys) {

      if (colors.isNotEmpty) {
        barChartGroupData.add(BarChartGroupData(
            x: key,
            barRods: [BarChartRodData(
                toY: barChartPoints[key]!,
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: colors
                )
            )]
        ));
      }
    }

    setState(() {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(barChartGroupData.isEmpty) {
      return const CircularProgressIndicator();
    }
    return Container(
      margin: const EdgeInsets.only(left: 10, top: 10, right: 50, bottom: 10),
      child: AspectRatio(
        aspectRatio: 2,
        child: BarChart(
            BarChartData(
              barGroups: barChartGroupData,
              borderData: FlBorderData(
                  border: const Border(bottom: BorderSide(), left: BorderSide())),
              gridData: const FlGridData(show: false),
              titlesData: FlTitlesData(
                bottomTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 20)),
                leftTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 40)),
                topTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
                rightTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
              ),
            )
        ),
      ),
    );
  }

  Widget customTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    if (value - 0.5 == value.toInt()) {
      return const Text('');
    }

    final intValue = value.toInt();

    return Padding(
      padding: const EdgeInsets.only(right: 6),
      child: Text(
        intValue.toString(),
        style: style,
        textAlign: TextAlign.center,
      ),
    );
  }
}