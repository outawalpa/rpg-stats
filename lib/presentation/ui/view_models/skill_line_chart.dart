import 'package:dice_stats/data/models/point.dart';
import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class SkillLineChartWidget extends StatefulWidget {

  final Skill skill;
  final Player player;
  final List<Roll> rolls;

  const SkillLineChartWidget(this.skill, this.player, this.rolls, {Key? key})
      : super(key: key);

  @override
  State<SkillLineChartWidget> createState() => _SkillLineChartWidgetState();
}

class _SkillLineChartWidgetState extends State<SkillLineChartWidget> {

  List<Point> lineChartPoints = [];

  @override
  void initState() {

    var index = 1;
    for (var roll in widget.rolls) {
      lineChartPoints.add(Point(x: index.toDouble(), y: roll.result.toDouble()));
      index++;
    }
    setState(() {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(lineChartPoints.isEmpty) {
      return const CircularProgressIndicator();
    }
    return Container(
      margin: const EdgeInsets.only(left: 10, top: 10, right: 50, bottom: 10),
      child: AspectRatio(
        aspectRatio: 2,
        child: LineChart(
          LineChartData(
            lineBarsData: [
              LineChartBarData(
                  spots: lineChartPoints.map((point) => FlSpot(point.x, point.y)).toList(),
                  isCurved: true,
                  color: Color(widget.player.color),
                  dotData: const FlDotData(show: false),
              )
            ],
            borderData: FlBorderData(
                border: const Border(bottom: BorderSide(), left: BorderSide())),
            gridData: const FlGridData(show: false),
            minY: 0,
            titlesData: FlTitlesData(
              bottomTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 20)),
              leftTitles: AxisTitles(sideTitles: SideTitles(showTitles: true, getTitlesWidget: customTitleWidgets, reservedSize: 40)),
              topTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
              rightTitles: const AxisTitles(sideTitles: SideTitles(showTitles: false)),
            ),
          ),
        ),
      ),
    );
  }

  Widget customTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    if (value - 0.5 == value.toInt()) {
      return const Text('');
    }

    final intValue = value.toInt();

    return Padding(
      padding: const EdgeInsets.only(right: 6),
      child: Text(
        intValue.toString(),
        style: style,
        textAlign: TextAlign.center,
      ),
    );
  }

}