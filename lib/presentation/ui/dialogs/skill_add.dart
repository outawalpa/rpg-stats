import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:dice_stats/presentation/ui/view_models/input_decoration.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SkillAddView extends StatefulWidget {
  const SkillAddView({super.key, required this.gameId});

  final int gameId;

  @override
  State<SkillAddView> createState() => _SkillAddViewState();
}

class _SkillAddViewState extends State<SkillAddView> {

  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final subtitleController = TextEditingController();

  int selectedSection = 0;
  List<Section> sections = [];

  @override
  void initState() {

    Section.queryAll(widget.gameId)
        .then((value) {
          sections = value;
          setState(() {});
        });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      title: const Text('Ajouter une compétence',
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [

              //Title of the skill
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                alignment: Alignment.centerLeft,
                child: const Text('Titre',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: TextFormField(
                  decoration: const CustomInputDecoration(hint: 'Titre'),
                  controller: titleController,
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Titre manquant';
                    }
                    return null;
                  },
                ),
              ),

              //Subtitle of the skill
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                alignment: Alignment.centerLeft,
                child: const Text('Sous-titre',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: TextFormField(
                  decoration: const CustomInputDecoration(hint: 'Sous-titre'),
                  controller: subtitleController,
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      subtitleController.text = '';
                    }
                    return null;
                  },
                ),
              ),

              // Section
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                alignment: Alignment.centerLeft,
                child: const Text('Section',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                    color: Colors.white
                ),
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                alignment: Alignment.center,
                child: DropdownButtonFormField(
                  decoration: const InputDecoration.collapsed(hintText: 'Section', fillColor: Colors.white),
                  isExpanded: true,
                  padding: const EdgeInsets.all(20),
                  dropdownColor: Colors.white,
                  onChanged: (int? newValue){
                    setState(() {
                      selectedSection = newValue!;
                    });
                  },
                  items: sectionList,
                  validator: (value) {
                    if (value == null || value == 0) {
                      return 'Selectionnez une section';
                    }
                    return null;
                  },
                ),
              ),

            ],
          ),
        ),
      ),
      actions: [
        // Cancel button
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red
          ),
          child: const Text('Annuler',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),

        // Validate button
        ElevatedButton(
          onPressed: () {
            if (_formKey.currentState!.validate() && selectedSection != 0) {
              Skill.insert(titleController.text, subtitleController.text, selectedSection)
                  .then((value) => Navigator.pop(context));
            } else if (selectedSection == 0) {
              Fluttertoast.showToast(
                  msg: "Selectionnez une section",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0
              );
            }
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.green
          ),
          child: const Text('Ajouter',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        )
      ],
    );
  }

  //Create sections list for Dropdown menu
  List<DropdownMenuItem<int>> get sectionList {
    List<DropdownMenuItem<int>> menuItems = [];

    for (var section in sections) {
      menuItems.add(DropdownMenuItem(value: section.id, child: Text(section.title)));
    }

    return menuItems;
  }

}