import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/presentation/ui/view_models/input_decoration.dart';
import 'package:flutter/material.dart';

import 'package:dice_stats/data/database_helper.dart';

class GameAddView extends StatefulWidget {
  const GameAddView({super.key});

  @override
  State<GameAddView> createState() => _GameAddViewState();
}

class _GameAddViewState extends State<GameAddView> {

  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final subtitleController = TextEditingController();

  final dbHelper = DatabaseHelper();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      title: const Text('Créer une partie',
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [

                  //Titre
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text('Titre',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17
                      ),
                    ),
                  ),
                  TextFormField(
                    decoration: const CustomInputDecoration(hint: 'Titre'),
                    controller: titleController,
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Titre manquant';
                      }
                      return null;
                    },
                  ),

                  //Sous-titre
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    alignment: Alignment.centerLeft,
                    child: const Text('Sous-titre',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17
                      ),
                    ),
                  ),
                  TextFormField(
                    decoration: const CustomInputDecoration(hint: 'Sous-titre'),
                    controller: subtitleController,
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        subtitleController.text = '';
                      }
                      return null;
                    },
                  ),

                ],
              ),
            ),
          ],
        ),
      ),
      actions: [
        // Cancel button
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red
          ),
          child: const Text('Retour',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),

        // Validate button
        ElevatedButton(
          onPressed: () async {
            if (_formKey.currentState!.validate()) {
              Game.insert(titleController.text, subtitleController.text)
                  .then((value) => Navigator.pop(context));
            }
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.green
          ),
          child: const Text('Ajouter',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        )
      ],
    );
  }
}