import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/presentation/ui/view_models/input_decoration.dart';
import 'package:flutter/material.dart';

class SectionAddView extends StatefulWidget {
  const SectionAddView({super.key, required this.gameId});

  final int gameId;

  @override
  State<SectionAddView> createState() => _SectionAddViewState();
}

class _SectionAddViewState extends State<SectionAddView> {

  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      title: const Text('Ajouter une section',
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [

              //Title of the skill
              Container(
                margin: const EdgeInsets.only(bottom: 10,),
                alignment: Alignment.centerLeft,
                child: const Text('Titre',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17
                  ),
                ),
              ),
              TextFormField(
                decoration: const CustomInputDecoration(hint: 'Titre'),
                controller: titleController,
                keyboardType: TextInputType.text,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Titre manquant';
                  }
                  return null;
                },
              ),

            ],
          ),
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.red,
          ),
          child: const Text('Annuler',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            if(_formKey.currentState!.validate()) {
              Section.insert(titleController.text, widget.gameId)
                .then((value) {
                  Navigator.pop(context);
                });
            }
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.green,
          ),
          child: const Text('Valider',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
      ],
    );

  }
}