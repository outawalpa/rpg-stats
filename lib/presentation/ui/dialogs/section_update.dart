import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/presentation/ui/view_models/input_decoration.dart';
import 'package:flutter/material.dart';

class SectionUpdateView extends StatefulWidget {
  const SectionUpdateView({super.key, required this.section});

  final Section section;

  @override
  State<SectionUpdateView> createState() => _SectionUpdateViewState();
}

class _SectionUpdateViewState extends State<SectionUpdateView> {

  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();

  @override
  void initState() {

    setState(() {
      titleController.text = widget.section.title;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      title: const Text('Modifier la section',
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [

              //Title of the skill
              Container(
                margin: const EdgeInsets.only(bottom: 10,),
                alignment: Alignment.centerLeft,
                child: const Text('Titre',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17
                  ),
                ),
              ),
              TextFormField(
                decoration: const CustomInputDecoration(hint: 'Titre'),
                controller: titleController,
                keyboardType: TextInputType.text,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Titre manquant';
                  }
                  return null;
                },
              ),

            ],
          ),
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.red,
          ),
          child: const Text('Annuler',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            if(_formKey.currentState!.validate()) {
              Section.update(widget.section.id, titleController.text, widget.section.gameId)
                  .then((value) {
                Navigator.pop(context);
              });
            }
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.blue,
          ),
          child: const Text('Modifier',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
      ],
    );

  }
}