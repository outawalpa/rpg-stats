import 'package:dice_stats/data/models/skill.dart';
import 'package:dice_stats/presentation/ui/view_models/input_decoration.dart';
import 'package:flutter/material.dart';

class SkillUpdateView extends StatefulWidget {
  const SkillUpdateView({super.key, required this.skill});

  final Skill skill;

  @override
  State<SkillUpdateView> createState() => _SkillUpdateViewState();
}

class _SkillUpdateViewState extends State<SkillUpdateView> {

  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final subtitleController = TextEditingController();

  @override
  void initState() {

    setState(() {
      titleController.text = widget.skill.title;
      subtitleController.text = widget.skill.subtitle;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      title: const Text('Ajouter une compétence',
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [

              //Title of the skill
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                alignment: Alignment.centerLeft,
                child: const Text('Titre',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: TextFormField(
                  decoration: const CustomInputDecoration(hint: 'Titre'),
                  controller: titleController,
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Titre manquant';
                    }
                    return null;
                  },
                ),
              ),

              //Subtitle of the skill
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                alignment: Alignment.centerLeft,
                child: const Text('Sous-titre',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: TextFormField(
                  decoration: const CustomInputDecoration(hint: 'Sous-titre'),
                  controller: subtitleController,
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      subtitleController.text = '';
                    }
                    return null;
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      actions: [
        // Cancel button
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red
          ),
          child: const Text('Annuler',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),

        // Validate button
        ElevatedButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              Skill.update(widget.skill.id, titleController.text, subtitleController.text, widget.skill.sectionId)
                  .then((value) => Navigator.pop(context));
            }
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blue
          ),
          child: const Text('Modifier',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        )
      ],
    );
  }
}