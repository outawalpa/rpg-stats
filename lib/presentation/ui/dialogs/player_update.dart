import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/presentation/ui/view_models/input_decoration.dart';
import 'package:flutter/material.dart';

import 'package:flutter_colorpicker/flutter_colorpicker.dart';

import 'package:dice_stats/data/database_helper.dart';

class PlayerUpdateView extends StatefulWidget {
  const PlayerUpdateView({super.key, required this.player});

  final Player player;

  @override
  State<PlayerUpdateView> createState() => _PlayerUpdateViewState();
}

class _PlayerUpdateViewState extends State<PlayerUpdateView> {

  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final subtitleController = TextEditingController();

  final dbHelper = DatabaseHelper();

  int selectedColor = 4294901760;

  @override
  void initState() {

    setState(() {
      titleController.text = widget.player.title;
      subtitleController.text = widget.player.subtitle;
      selectedColor = widget.player.color;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      title: const Text('Ajouter un joueur',
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              Form(
                key: _formKey,
                child: Column(
                  children: [

                    // Name
                    Container(
                      alignment: Alignment.centerLeft,
                      child: const Text('Joueur',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17
                        ),
                      ),
                    ),
                    TextFormField(
                      decoration: const CustomInputDecoration(hint: 'Joueur'),
                      controller: titleController,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Nom de joueur requis';
                        }
                        return null;
                      },
                    ),

                    // Name
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      alignment: Alignment.centerLeft,
                      child: const Text('Personnage (facultatif)',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17
                        ),
                      ),
                    ),
                    TextFormField(
                      decoration: const CustomInputDecoration(hint: 'Personnage'),
                      controller: subtitleController,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          subtitleController.text = '';
                        }
                        return null;
                      },
                    ),

                    //Selected color
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      alignment: Alignment.centerLeft,
                      child: const Text('Couleur',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17
                        ),
                      ),
                    ),
                    BlockPicker(
                      availableColors: const [
                        Color(4294901760),
                        Color(4278255360),
                        Color(4278190335),
                        Color(4294902015),
                        Color(4294967040),
                        Color(4278255615),
                      ],
                      pickerColor: Color(selectedColor), //default color
                      onColorChanged: (Color color) { //on the color picked
                        selectedColor = color.value;
                      },
                    )


                  ],
                ),
              ),
            ],
          )
      ),
      actions: [
        // Cancel button
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red
          ),
          child: const Text('Retour',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),

        // Validate button
        ElevatedButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              Player.update(widget.player.id, titleController.text, subtitleController.text, selectedColor, widget.player.gameId)
                  .then((value) => Navigator.pop(context));
            }
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blue
          ),
          child: const Text('Modifier',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        )
      ],
    );
  }
}