import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:dice_stats/presentation/ui/view_models/section_pie_chart.dart';
import 'package:dice_stats/presentation/ui/view_models/section_pie_chart_joined.dart';
import 'package:flutter/material.dart';

import 'package:dice_stats/data/database_helper.dart';

class SectionStatisticView extends StatefulWidget {
  const SectionStatisticView({super.key, required this.gameId, required this.section});

  final int gameId;
  final Section section;

  @override
  State<SectionStatisticView> createState() => _SectionStatisticViewState();
}

class _SectionStatisticViewState extends State<SectionStatisticView> {

  final dbHelper = DatabaseHelper();

  List<Player> players = [];
  List<Skill> skills = [];
  Map<int, List<Roll>> rolls = {};

  @override
  void initState() {

    Player.queryAll(widget.gameId).then((value) {
      players = value;
      Skill.queryAll(widget.section.id).then((value) {
        skills = value;
        getRolls();
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Center(
            child: (players.isEmpty || skills.isEmpty || rolls.isEmpty) ? const Text("Aucune donnée n'est enregistrée") : ListView.separated(
              separatorBuilder: (context, index) => const Divider(
                color: Colors.black,
                indent: 20,
                endIndent: 20,
              ),
              shrinkWrap: true,
              itemCount: players.length + 1,
              itemBuilder: (context, index) {
                if( index == 0 && players.length > 1) {
                  return Container(
                    margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                    child: Column(
                      children: [

                        Text(widget.section.title,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold
                          ),
                        ),

                        // Pie chart
                        SectionPieChartJoinedWidget(widget.section, skills, rolls),

                      ],
                    ),
                  );
                } else  {

                  return Container(
                    margin: const EdgeInsets.all(20),
                    child: Column(
                      children: [

                        // Name
                        Text(players[index-1].title,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold
                          ),
                        ),

                        // Pie chart
                        SectionPieChartWidget(widget.section, skills, rolls[players[index-1].id]!),

                      ],
                    ),
                  );
                }
              },
            ),
          ),
        )
    );
  }

  getRolls() async {
    for (var player in players) {
      rolls[player.id] = [];
      for (var skill in skills) {
        await Roll.queryAllForPlayer(skill.id, player.id).then((value) {
          rolls[player.id]!.addAll(value);
        });
      }
    }

    setState(() {

    });
  }
}