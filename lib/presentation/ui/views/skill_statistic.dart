import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_bar_chart.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_bar_chart_separated.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_line_chart_separated.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_line_chart.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_bar_chart_joined.dart';
import 'package:flutter/material.dart';

import 'package:dice_stats/data/database_helper.dart';

class SkillStatisticView extends StatefulWidget {
  const SkillStatisticView({super.key, required this.gameId, required this.skill});

  final int gameId;
  final Skill skill;

  @override
  State<SkillStatisticView> createState() => _SkillStatisticViewState();
}

class _SkillStatisticViewState extends State<SkillStatisticView> {

  final dbHelper = DatabaseHelper();

  List<Player> players = [];
  List<Roll> allRolls = [];
  Map<int, List<Roll>> rolls = {};

  @override
  void initState() {

    Player.queryAll(widget.gameId)
        .then((value) {
          players = value;
          setState(() {});
    });

    Roll.queryAll(widget.skill.id)
        .then((value) {

          for (var player in players) {
            if (rolls[player.id] == null) { rolls[player.id] = []; }
          }

          allRolls = value;
          for (var roll in value) {
            if (rolls[roll.playerId] == null) { rolls[roll.playerId] = []; }
            rolls[roll.playerId]?.add(roll);
          }
          setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Center(
            child: (players.isEmpty || rolls.isEmpty || allRolls.isEmpty) ? const Text("Aucune donnée n'est enregistrée") : ListView.builder(
              shrinkWrap: true,
              itemCount: players.length + 1,
              itemBuilder: (context, index) {
                if( index == 0 && players.length > 1) {
                  return Container(
                    margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                    child: Column(
                      children: [

                        Text(widget.skill.title,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold
                          ),
                        ),

                        // Line chart
                        SkillLineChartSeparatedWidget(widget.skill, players, rolls),

                        // Bar chart
                        SkillBarChartSeparatedWidget(widget.skill, players, rolls),

                        // Bar chart total
                        SkillBarChartJoinedWidget(widget.skill, players, allRolls),

                        const Divider(
                          color: Colors.black,
                          indent: 0,
                          endIndent: 0,
                        ),

                      ],
                    ),
                  );
                }
                else if (rolls[players[index-1].id] != null && rolls[players[index-1].id]!.isNotEmpty) {
                    return Container(
                    margin: const EdgeInsets.all(20),
                    child: Column(
                      children: [

                        // Name
                        Text(players[index-1].title,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold
                          ),
                        ),

                        // Line chart
                        SkillLineChartWidget(widget.skill, players[index-1], rolls[index]!),

                        // Bar chart
                        SkillBarChartWidget(widget.skill, players[index-1], rolls[index]!),

                        const Divider(
                          color: Colors.black,
                          indent: 0,
                          endIndent: 0,
                        ),

                      ],
                    ),
                  );
                }
                return const SizedBox(height: 0);
              },
            ),
          ),
        )
    );
  }

}