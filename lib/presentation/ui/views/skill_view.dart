import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:dice_stats/presentation/ui/dialogs/section_add.dart';
import 'package:dice_stats/presentation/ui/dialogs/skill_add.dart';
import 'package:dice_stats/presentation/ui/dialogs/skill_update.dart';
import 'package:dice_stats/presentation/ui/views/section_statistic.dart';
import 'package:dice_stats/presentation/ui/dialogs/section_update.dart';
import 'package:dice_stats/presentation/ui/views/skill_statistic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:dice_stats/data/database_helper.dart';

class SkillView extends StatefulWidget {
  const SkillView({super.key, required this.game});

  final Game game;

  @override
  State<SkillView> createState() => _SkillViewState();
}

class _SkillViewState extends State<SkillView> {

  final dbHelper = DatabaseHelper();
  List<dynamic> sections = [];

  @override
  void initState() {
    Section.queryAll(widget.game.id)
      .then((value) {
        for(var section in value) {
          Skill.queryAll(section.id)
            .then((value) {
              sections.add(section);
              for(var skill in value) {
                sections.add(skill);
              }
              setState(() {});
            });
        }
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: const Color(0xFF061c37),
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                if (sections.isNotEmpty)  Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: sections.length,
                        itemBuilder: (context, index){
                          return Column(
                            children: [

                              if (sections[index] is Section) Slidable(
                                // Add actions on slide
                                endActionPane: ActionPane(
                                  motion: const ScrollMotion(),
                                  children: [

                                    // Watch statistics
                                    SlidableAction(
                                      onPressed: (context) {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => SectionStatisticView(gameId: widget.game.id, section: sections[index]))
                                        );
                                      },
                                      backgroundColor: Colors.green,
                                      foregroundColor: const Color(0xFFFFFFFF),
                                      icon: Icons.bar_chart,
                                    ),

                                    // Update the section
                                    SlidableAction(
                                      onPressed: (context) {
                                        showDialog(
                                            context: context,
                                            builder: (context) {
                                              return SectionUpdateView(section: sections[index]);
                                            }
                                        ).then((value) {
                                          Section.queryAll(widget.game.id)
                                              .then((value) {
                                            sections = [];
                                            for(var section in value) {
                                              Skill.queryAll(section.id)
                                                  .then((value) {
                                                sections.add(section);
                                                for(var skill in value) {
                                                  sections.add(skill);
                                                }
                                                setState(() {});
                                              });
                                            }

                                          });
                                        });
                                      },
                                      backgroundColor: Colors.blue,
                                      foregroundColor: const Color(0xFFFFFFFF),
                                      icon: Icons.edit_note,
                                    ),

                                    // Delete the section
                                    SlidableAction(
                                      onPressed: (context) {
                                        Section.delete(sections[index].id)
                                          .then((value) {
                                            Section.queryAll(widget.game.id)
                                              .then((value) {
                                                for(var section in value) {
                                                  sections.add(section);
                                                  Skill.queryAll(section.id)
                                                    .then((value) {
                                                      for(var skill in value) {
                                                        sections.add(skill);
                                                      }
                                                    });
                                                }
                                              setState(() {});
                                            });
                                          });
                                      },
                                      backgroundColor: Colors.red,
                                      foregroundColor: const Color(0xFFFFFFFF),
                                      icon: Icons.delete,
                                    ),
                                  ]
                                ),

                                child: Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(
                                    color: Colors.blueGrey
                                  ),
                                  child: ListTile(
                                      title: Text(sections[index].title,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold
                                        ),
                                      )
                                  ),

                                ),
                              ),

                              if (sections[index] is Skill) Slidable(

                                // Add actions on slide
                                endActionPane: ActionPane(
                                    motion: const ScrollMotion(),
                                    children: [

                                      // Watch statistics
                                      SlidableAction(
                                        onPressed: (context) {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => SkillStatisticView(gameId: widget.game.id, skill: sections[index]))
                                          );
                                        },
                                        backgroundColor: Colors.green,
                                        foregroundColor: const Color(0xFFFFFFFF),
                                        icon: Icons.bar_chart,
                                      ),

                                      // Update the section
                                      SlidableAction(
                                        onPressed: (context) {
                                          showDialog(
                                              context: context,
                                              builder: (context) {
                                                return SkillUpdateView(skill: sections[index]);
                                              }
                                          ).then((value) {
                                            Section.queryAll(widget.game.id)
                                                .then((value) {
                                              sections = [];
                                              for(var section in value) {
                                                Skill.queryAll(section.id)
                                                    .then((value) {
                                                  sections.add(section);
                                                  for(var skill in value) {
                                                    sections.add(skill);
                                                  }
                                                  setState(() {});
                                                });
                                              }

                                            });
                                          });
                                        },
                                        backgroundColor: Colors.blue,
                                        foregroundColor: const Color(0xFFFFFFFF),
                                        icon: Icons.edit_note,
                                      ),

                                      // Delete the skill
                                      SlidableAction(
                                        onPressed: (context) {
                                          Skill.delete(sections[index].id)
                                            .then((value) {
                                              sections = [];
                                              Section.queryAll(widget.game.id)
                                                .then((value) {
                                                  for(var section in value) {
                                                    Skill.queryAll(section.id)
                                                      .then((value) {
                                                        sections.add(section);
                                                        for(var skill in value) {
                                                          sections.add(skill);
                                                        }
                                                        setState(() {});
                                                    });
                                                  }

                                              });
                                          });
                                        },
                                        backgroundColor: Colors.red,
                                        foregroundColor: const Color(0xFFFFFFFF),
                                        icon: Icons.delete,
                                      ),
                                    ]
                                ),

                                child: Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.all(0),
                                  decoration: const BoxDecoration(
                                    //color: Color(0xFF2f353b)
                                  ),
                                  child: ListTile(
                                    title: Text(sections[index].title),
                                    subtitle: sections[index].subtitle != '' ? Text(sections[index].subtitle) : null,
                                  ),

                                ),
                              )
                            ],
                          );
                        }
                    )
                )
                else Expanded(child: Center(child: Image.asset('assets/images/d100.png'))),

                // Create a new skill
                Container(
                  margin: const EdgeInsets.all(20),
                  child: ElevatedButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: const Text('Ajouter...',
                              textAlign: TextAlign.center,
                            ),
                            actions: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return SectionAddView(gameId: widget.game.id);
                                      }
                                  ).then((value) {
                                    Section.queryAll(widget.game.id)
                                      .then((value) {
                                        sections = [];
                                        for(var section in value) {
                                          Skill.queryAll(section.id)
                                            .then((value) {
                                              sections.add(section);
                                              for(var skill in value) {
                                                sections.add(skill);
                                              }
                                              setState(() {});
                                            });
                                        }

                                    });
                                  });
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.green,
                                ),
                                child: const Text('une section.',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  if (sections.isNotEmpty) {
                                    Navigator.pop(context);
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return SkillAddView(gameId: widget.game.id);
                                        }
                                    ).then((value) {
                                      Section.queryAll(widget.game.id)
                                          .then((value) {
                                            sections = [];
                                        for(var section in value) {
                                          Skill.queryAll(section.id)
                                              .then((value) {
                                            sections.add(section);
                                            for(var skill in value) {
                                              sections.add(skill);
                                            }
                                            setState(() {});
                                          });
                                        }

                                      });
                                    });
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: sections.isNotEmpty ? Colors.green : Colors.white30,
                                ),
                                child: const Text('une compétence.',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.red,
                                ),
                                child: const Text('rien du tout !',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              )
                            ],
                          );
                        }
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.green,
                    ),
                    child: const Text('Ajouter',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                )

              ],
            ),
          ),
        )  // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}