import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:dice_stats/data/database_helper.dart';


class LastRollView extends StatefulWidget {
  const LastRollView({super.key, required this.game});

  final Game game;

  @override
  State<LastRollView> createState() => _LastRollViewState();
}

class _LastRollViewState extends State<LastRollView> {

  final dbHelper = DatabaseHelper();

  int selectedPlayer = 0;
  List<Player> players = [];

  int selectedSkill = 0;
  List<Skill> skills = [];

  int rollLimit = 5;
  List<Roll> rolls = [];

  @override
  void initState() {
    Player.queryAll(widget.game.id)
        .then((value) {
      players = value;
      setState(() {});
    });

    Section.queryAll(widget.game.id)
        .then((value) {
      for(var section in value) {
        Skill.queryAll(section.id)
            .then((value) {
          for(var skill in value) {
            skills.add(skill);
          }
          setState(() {});
        });
      }
    });

    Roll.queryLast(5)
        .then((value) {
      for (var roll in value) {
        rolls.add(roll);
      }
      setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Derniers lancés',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20
                      ),
                    ),
                    Row(
                      children: [

                        InkWell(
                          onTap: () {
                            if(rollLimit != 5) {
                              rollLimit = 5;
                              Roll.queryLast(5)
                                  .then((value) {
                                rolls = [];
                                for (var roll in value) {
                                  rolls.add(roll);
                                }
                                setState(() {});
                              });
                            }
                          },
                          child: Container(
                            width: 40,
                            height: 40,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              border: Border.all(),
                              color: rollLimit == 5 ? Colors.blueGrey : Colors.white,
                            ),
                            child: Text('5',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: rollLimit == 5 ? Colors.white : Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ),
                        ),

                        InkWell(
                          onTap: () {
                            if(rollLimit != 10) {
                              rollLimit = 10;
                              Roll.queryLast(10)
                                  .then((value) {
                                rolls = [];
                                for (var roll in value) {
                                  rolls.add(roll);
                                }
                                setState(() {});
                              });
                            }
                          },
                          child: Container(
                            width: 40,
                            height: 40,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              border: Border.all(),
                              color: rollLimit == 10 ? Colors.blueGrey : Colors.white,
                            ),
                            child: Text('10',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: rollLimit == 10 ? Colors.white : Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ),
                        ),

                        InkWell(
                          onTap: () {
                            if(rollLimit != 0) {
                              rollLimit = 0;
                              Roll.queryLast(0)
                                  .then((value) {
                                rolls = [];
                                for (var roll in value) {
                                  rolls.add(roll);
                                }
                                setState(() {});
                              });
                            }
                          },
                          child: Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              border: Border.all(),
                              color: rollLimit == 0 ? Colors.blueGrey : Colors.white,
                            ),
                            child: Icon(Icons.all_inclusive, color: rollLimit == 0 ? Colors.white : Colors.black,),
                          ),
                        ),

                      ],
                    )
                  ],
                ),

                rolls.isEmpty ? Image.asset('assets/images/d100.png') : Container(
                  padding: const EdgeInsets.only(top: 20),
                  child: ListView.builder(
                      physics : const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: rolls.length,
                      itemBuilder: (context, index){

                        if(index == 0) {
                          return Container(
                            padding: const EdgeInsets.all(5),
                            decoration: const BoxDecoration(
                                color: Colors.blueGrey
                            ),
                            child: const Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Flexible(
                                    fit: FlexFit.tight,
                                    flex: 4,
                                    child: Text('Nom',
                                      style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold
                                      ),
                                    )
                                ),
                                Flexible(
                                    fit: FlexFit.tight,
                                    flex: 4,
                                    child: Text('Compétence',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold
                                      ),
                                    )
                                ),
                                Flexible(
                                    fit: FlexFit.tight,
                                    flex: 2,
                                    child: Text('Résultat',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold
                                      ),
                                    )
                                ),
                              ],
                            ),
                          );
                        } else {
                          return Column(
                            children: [
                              Slidable(

                                // Add actions on slide
                                  endActionPane: ActionPane(
                                      motion: const ScrollMotion(),
                                      children: [

                                        // Delete the roll
                                        SlidableAction(
                                          onPressed: (context) {
                                            Roll.delete(rolls[index].id)
                                                .then((value) => Roll.queryLast(rollLimit))
                                                .then((value) {
                                              rolls = [];
                                              for (var roll in value) {
                                                rolls.add(roll);
                                              }
                                              setState(() {});
                                            });
                                          },
                                          backgroundColor: Colors.red,
                                          foregroundColor: const Color(0xFFFFFFFF),
                                          icon: Icons.delete,
                                        ),
                                      ]
                                  ),

                                  // Access to the game onTap
                                  child: Container(
                                    margin: const EdgeInsets.only(left: 5, top: 10, right: 5, bottom: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        if (players.isNotEmpty) Flexible(
                                            fit: FlexFit.tight,
                                            flex: 4,
                                            child: Text(players.where((e) => e.id == rolls[index].playerId).first.title,
                                              style: const TextStyle(
                                                fontSize: 15,
                                              ),
                                            )
                                        ),
                                        if (skills.isNotEmpty) Flexible(
                                            fit: FlexFit.tight,
                                            flex: 4,
                                            child: Text(skills.where((e) => e.id == rolls[index].skillId).first.title,
                                              style: const TextStyle(
                                                  fontSize: 15
                                              ),
                                            )
                                        ),
                                        if (rolls.isNotEmpty) Flexible(
                                            fit: FlexFit.tight,
                                            flex: 2,
                                            child: Text(rolls[index].result.toString(),
                                              style: const TextStyle(
                                                  fontSize: 15
                                              ),
                                            )
                                        ),
                                      ],
                                    ),
                                  )
                              ),
                            ],
                          );
                        }
                      }
                  ),
                )
              ],
            ),
          ),
        )
    );
  }
}