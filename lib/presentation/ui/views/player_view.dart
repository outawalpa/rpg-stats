import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/presentation/ui/dialogs/player_add.dart';
import 'package:dice_stats/presentation/ui/dialogs/player_update.dart';
import 'package:flutter/material.dart';

import 'package:dice_stats/data/database_helper.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class PlayerView extends StatefulWidget {
  const PlayerView({super.key, required this.game});

  final Game game;

  @override
  State<PlayerView> createState() => _PlayerViewState();
}

class _PlayerViewState extends State<PlayerView> {

  final dbHelper = DatabaseHelper();

  final playerController = TextEditingController();
  final colorController = TextEditingController();

  List<Player> players = [];

  @override
  void initState() {
    Player.queryAll(widget.game.id)
      .then((value) {
        players = value;
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: const Color(0xFF061c37),
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [

                if (players.isNotEmpty) Expanded(
                    child: ListView.separated(
                        separatorBuilder: (context, index) => const Divider(
                          color: Colors.black,
                          indent: 20,
                          endIndent: 20,
                        ),
                        shrinkWrap: true,
                        itemCount: players.length,
                        itemBuilder: (context, index){
                          return Column(
                            children: [

                              // Display the game with options
                              Slidable(

                                // Add actions on slide
                                  endActionPane: ActionPane(
                                      motion: const ScrollMotion(),
                                      children: [

                                        // Update the player
                                        SlidableAction(
                                          onPressed: (context) {
                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return PlayerUpdateView(player: players[index]);
                                                }
                                            ).then((value) => Player.queryAll(widget.game.id).then((value) {
                                              players = value;
                                              setState(() {});
                                            }));
                                          },
                                          backgroundColor: Colors.blue,
                                          foregroundColor: const Color(0xFFFFFFFF),
                                          icon: Icons.edit_note,
                                        ),

                                        // Delete the player
                                        SlidableAction(
                                          onPressed: (context) {
                                            Player.delete(players[index].id)
                                                .then((value) => Player.queryAll(widget.game.id).then((value) {
                                                  players = value;
                                                  setState(() {});
                                                }));
                                          },
                                          backgroundColor: Colors.red,
                                          foregroundColor: const Color(0xFFFFFFFF),
                                          icon: Icons.delete,
                                        ),
                                      ]
                                  ),

                                child: ListTile(
                                  leading: Icon(Icons.circle, color: Color(players[index].color),),
                                  title: Text(players[index].title),
                                  subtitle: players[index].subtitle != '' ? Text(players[index].subtitle) : null,
                                ),
                              )
                            ],
                          );
                        }
                    )
                )
                else Expanded(child: Center(child: Image.asset('assets/images/d100.png'))),

                Container(
                  margin: const EdgeInsets.all(20),
                  child: ElevatedButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return PlayerAddView(gameId: widget.game.id);
                          }
                      ).then((value) => Player.queryAll(widget.game.id)
                          .then((value) {
                            players = value;
                            setState(() {});
                          })
                      );
                    },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.green
                    ),
                    child: const Text('Nouveau joueur',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        )  // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}