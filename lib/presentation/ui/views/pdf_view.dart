import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/roll.dart';

import 'package:dice_stats/data/database_helper.dart';
import 'package:dice_stats/presentation/ui/view_models/section_pie_chart.dart';
import 'package:dice_stats/presentation/ui/view_models/section_pie_chart_joined.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_bar_chart.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_bar_chart_joined.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_bar_chart_separated.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_line_chart.dart';
import 'package:dice_stats/presentation/ui/view_models/skill_line_chart_separated.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'dart:io';
import 'package:screenshot/screenshot.dart';

class PdfView extends StatefulWidget {
  const PdfView({super.key, required this.game});

  final Game game;

  @override
  State<PdfView> createState() => _PdfViewState();
}

class _PdfViewState extends State<PdfView> {

  final dbHelper = DatabaseHelper();

  final pdf = pw.Document();
  final screenshotController = ScreenshotController();

  List<Section> sections = [];
  Map<int, List<Skill>> skills = {};
  List<Player> players = [];
  Map<int, List<Roll>> mapPlayerRolls = {};
  Map<int, List<Roll>> mapSkillRolls = {};

  int steps = 0;
  int toDo = 0;
  int done = 0;

  @override
  void initState() {

    askPermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Dialog(
      child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.all(20),
                child: const CircularProgressIndicator(),
              ),
              if(steps == 0 ) const Text('Permissions requises'),
              if(steps == 1 ) const Text('Récupération des données'),
              if(steps == 2 ) Text('$done / $toDo'),
              if(steps == 3 ) const Text('Sauvegarde'),
            ],
          )
      ),
    );
  }

  askPermission() async {
    if (await Permission.storage.isGranted && await Permission.manageExternalStorage.isGranted) {
      queryAll();
    } else {
      await [Permission.storage, Permission.manageExternalStorage].request();
      if (await Permission.storage.isGranted && await Permission.manageExternalStorage.isGranted) {
        queryAll();
      } else {
        permissionDenied();
      }
    }
  }

  permissionDenied() {
    Fluttertoast.showToast(
        msg: "Permission requise",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    Navigator.pop(context);
  }

  queryAll() async {

    setState(() { steps = 1; });

    players = await Player.queryAll(widget.game.id);
    sections = await Section.queryAll(widget.game.id);
    for(var section in sections) {
      setState(() { toDo += 1; });
      skills[section.id] = [];
      skills[section.id]!.addAll(await Skill.queryAll(section.id));
      for(var skill in skills[section.id]!) {
        setState(() { toDo += 1; });
        mapSkillRolls[skill.id] = await Roll.queryAll(skill.id);

        for(var player in players) {
          setState(() { toDo += 1; });
          if(mapPlayerRolls[player.id] == null) {mapPlayerRolls[player.id] = [];}
          mapPlayerRolls[player.id]?.addAll(await Roll.queryAllForPlayer(skill.id, player.id)) ;
        }
      }
    }

    createPdf();

  }

  createPdf() async {

    setState(() { steps = 2; });

    final logo = (await rootBundle.load('assets/images/icon.png')).buffer.asUint8List();

    // First page
    pdf.addPage(
      pw.Page(
        pageFormat: PdfPageFormat.a4,
        margin: const pw.EdgeInsets.all(32),
        build: (pw.Context context) {
          return pw.Center(
            child: pw.Column(
                children: [
                  pw.Image(pw.MemoryImage(logo), width: 200, height: 200),
                  pw.Text('DX STATS')
                ]
            )
          );
        }
      )
    );

    // Index page
    pdf.addPage(
        pw.Page(
            pageFormat: PdfPageFormat.a4,
            margin: const pw.EdgeInsets.all(32),
            build: (pw.Context context) {
              return pw.Column(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  for(var section in sections) pw.Column(
                    crossAxisAlignment: pw.CrossAxisAlignment.start,
                    children: [
                      pw.Text(section.title,
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold
                        )
                      ),
                      for(var skill in skills[section.id]!) pw.Text('          ${skill.title} | ${skill.subtitle}')
                    ]
                  )
                ]
              );

            }
        )
    );


    for(var section in sections) {

      // Image of section statistics
      final sectionPieChartJoined = await screenshotController.captureFromWidget(MediaQuery(data: const MediaQueryData(), child: SectionPieChartJoinedWidget(section, skills[section.id]!, mapPlayerRolls)));

      // List Images of section statistics for each player
      final Map<int, Uint8List> sectionPieChartList = {};
      for(var player in players) {
        sectionPieChartList[player.id] = await screenshotController.captureFromWidget(MediaQuery(data: const MediaQueryData(), child: SectionPieChartWidget(section, skills[section.id]!, mapPlayerRolls[player.id]!)));
      }

      // Section statistics
      pdf.addPage(
          pw.Page(
              pageFormat: PdfPageFormat.a4,
              margin: const pw.EdgeInsets.all(32),
              build: (pw.Context context) {
                return pw.Column(
                    children: [
                      pw.Text(section.title),
                      pw.Image(pw.MemoryImage(sectionPieChartJoined)),
                      for(var player in players) pw.Column(
                          children: [
                            pw.Text(player.title),
                            if (player.subtitle != '') pw.Text(player.subtitle),
                            pw.Image(pw.MemoryImage(sectionPieChartList[player.id]!)),
                          ]
                      )
                    ]
                );
              }
          )
      );

      for(var skill in skills[section.id]!) {

        // Image of skill statistics
        final skillBarChartJoined = await screenshotController.captureFromWidget(MediaQuery(data: const MediaQueryData(), child: SkillBarChartJoinedWidget(skill, players, mapSkillRolls[skill.id]!)));
        final skillBarChartSeparated = await screenshotController.captureFromWidget(MediaQuery(data: const MediaQueryData(), child: SkillBarChartSeparatedWidget(skill, players, mapPlayerRolls)));
        final skillLineChartSeparated = await screenshotController.captureFromWidget(MediaQuery(data: const MediaQueryData(), child: SkillLineChartSeparatedWidget(skill, players, mapPlayerRolls)));

        // List Images of skill statistics for each player
        final Map<int, Uint8List> skillLineChartList = {};
        for(var player in players) {
          skillLineChartList[player.id] = await screenshotController.captureFromWidget(MediaQuery(data: const MediaQueryData(), child: SkillLineChartWidget(skill, player, mapPlayerRolls[player.id]!)));
        }

        // List Images of skill statistics for each player
        final Map<int, Uint8List> skillBarChartList = {};
        for(var player in players) {
          skillBarChartList[player.id] = await screenshotController.captureFromWidget(MediaQuery(data: const MediaQueryData(), child: SkillBarChartWidget(skill, player, mapPlayerRolls[player.id]!)));
        }

        pdf.addPage(
            pw.MultiPage(
                pageFormat: PdfPageFormat.a4,
                margin: const pw.EdgeInsets.all(32),
                build: (pw.Context context) {
                  for(var skill in skills[section.id]!) {
                    return <pw.Widget>[
                      pw.Column(
                          children: [
                            pw.Text(skill.title), // Title of the skill
                            if (skill.subtitle != '') pw.Text(skill.subtitle), // Subtitle of the skill if exist
                            pw.Image(pw.MemoryImage(skillBarChartJoined)),
                            pw.Image(pw.MemoryImage(skillBarChartSeparated)),
                            pw.Image(pw.MemoryImage(skillLineChartSeparated)),
                            for(var player in players) pw.Column(
                                children: [
                                  pw.Text(player.title),
                                  if (player.subtitle != '') pw.Text(player.subtitle),
                                  pw.Image(pw.MemoryImage(skillLineChartList[player.id]!)),
                                  pw.Image(pw.MemoryImage(skillBarChartList[player.id]!)),
                                ]
                            )
                          ]
                      )
                    ];
                  }
                  return <pw.Widget>[];
                }
            )
        );
      }


    }

    savePdf(pdf);
  }

  savePdf(pw.Document pdf) async {

    setState(() { steps = 3; });

    late File file;
    Directory directory;

    String path = '';

    if (Platform.isAndroid) {
      String androidPath = (await getExternalStorageDirectory())!.path;
      path = "$androidPath/dxStats.pdf";
    } else if (Platform.isIOS) {
      Directory iosPath = await getApplicationDocumentsDirectory();
      directory = await Directory("${iosPath.path}/dx_stats").create();
      path = "${directory.path}/dxStats.pdf";
    }

    file = File(path);

    if (await file.exists()) {
      try {
        await file.delete();
      } on Exception catch (e) {
        onError();
      }
    }

    await file.writeAsBytes(await pdf.save());
    // await send(file.path, context);

    onSuccess(path);

  }

  onError() {
    Fluttertoast.showToast(
        msg: "Erreur lors de la sauvegade",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    Navigator.pop(context);
  }

  onSuccess(String path) {
    Fluttertoast.showToast(
        msg: "Sauvegardé sous $path",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );

    Navigator.pop(context);
  }

  /*send(String path, var context) async {
    final Email email = Email(
      subject: "Mood Chart",
      recipients: [""],
      attachmentPaths: [path],
      isHTML: false,
    );

    try {
      await FlutterEmailSender.send(email);
      // platformResponse = 'Επιτυχία';
    } on PlatformException catch (error) {
      if (error.message == "No email clients found!") {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("Device has no email"),
            duration: Duration(seconds: 2)));
      }
    }
  }*/
}