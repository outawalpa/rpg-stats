import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/presentation/ui/dialogs/game_update.dart';
import 'package:dice_stats/presentation/ui/views/pdf_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:dice_stats/data/database_helper.dart';

import 'game_view.dart';
import '../dialogs/game_add.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {

  final dbHelper = DatabaseHelper();
  List<Game> games = [];

  @override
  void initState() {
    Game.queryAll().then((value) {
      games = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: const Color(0xFF061c37),
      body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [

                Image.asset('assets/images/icon.png', width: 200),
                const Text('RPG STATS',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold
                  ),
                ),

                if (games.isNotEmpty) Expanded(
                    child: ListView.separated(
                        separatorBuilder: (context, index) => const Divider(
                          color: Colors.black,
                          indent: 20,
                          endIndent: 20,
                        ),
                        shrinkWrap: true,
                        itemCount: games.length,
                        itemBuilder: (context, index){
                          return Column(
                            children: [

                              // Display the game with options
                              Slidable(

                                // Add actions on slide
                                  endActionPane: ActionPane(
                                      motion: const ScrollMotion(),
                                      children: [

                                        // Send all information in pdf with email
                                        SlidableAction(
                                          onPressed: (context) {
                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return PdfView(game: games[index]);
                                                }
                                            );
                                          },
                                          backgroundColor: Colors.blue.shade900,
                                          foregroundColor: const Color(0xFFFFFFFF),
                                          icon: Icons.save_alt,
                                        ),

                                        // Update the game
                                        SlidableAction(
                                          onPressed: (context) {
                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return GameUpdateView(game: games[index]);
                                                }
                                            ).then((value) => Game.queryAll()
                                                .then((value) {
                                              games = value;
                                              setState(() {});
                                            }));
                                          },
                                          backgroundColor: Colors.blue,
                                          foregroundColor: const Color(0xFFFFFFFF),
                                          icon: Icons.edit_note,
                                        ),

                                        // Delete the game
                                        SlidableAction(
                                          onPressed: (context) {
                                            Game.delete(games[index].id)
                                              .then((value) => Game.queryAll()
                                                .then((value) {
                                                  games = value;
                                                  setState(() {});
                                                }));
                                          },
                                          backgroundColor: Colors.red,
                                          foregroundColor: const Color(0xFFFFFFFF),
                                          icon: Icons.delete,
                                        ),
                                      ]
                                  ),

                                  // Access to the game onTap
                                  child: InkWell(
                                    child: Container(
                                      width: double.infinity,
                                      padding: const EdgeInsets.all(0),
                                      decoration: const BoxDecoration(
                                          //color: Color(0xFF2f353b)
                                      ),
                                      child: ListTile(
                                        title: Text(games[index].title,
                                          style: const TextStyle(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        subtitle: games[index].subtitle == '' ?  null : Text(games[index].subtitle,
                                          style: const TextStyle(
                                            color: Colors.black,
                                            fontSize: 15
                                          ),
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => GameView(game: games[index]))
                                          ).then((value) => Game.queryAll()
                                              .then((value) {
                                                games = value;
                                                setState(() {});
                                              })
                                          );

                                        },
                                      ),
                                    ),
                                  )
                              )
                            ],
                          );
                        }
                    )
                ),

                // Create a new game
                Container(
                  margin: const EdgeInsets.all(20),
                  child: ElevatedButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const GameAddView();
                        },

                      ).then((value) => Game.queryAll().then((value) {
                        games = value;
                        setState(() {});
                      }));
                    },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.green
                    ),
                    child: const Text('Nouveau jeu',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                )

              ],
            ),
          ),
      )  // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}