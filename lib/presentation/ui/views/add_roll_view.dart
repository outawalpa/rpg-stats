import 'package:dice_stats/data/models/roll.dart';
import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/data/models/player.dart';
import 'package:dice_stats/data/models/section.dart';
import 'package:dice_stats/data/models/skill.dart';
import 'package:dice_stats/presentation/ui/view_models/input_decoration.dart';
import 'package:flutter/material.dart';

import 'package:fluttertoast/fluttertoast.dart';

import 'package:dice_stats/data/database_helper.dart';


class AddRollView extends StatefulWidget {
  const AddRollView({super.key, required this.game});

  final Game game;

  @override
  State<AddRollView> createState() => _AddRollViewState();
}

class _AddRollViewState extends State<AddRollView> {

  final dbHelper = DatabaseHelper();

  final _formKey = GlobalKey<FormState>();
  final resultController = TextEditingController();

  int selectedPlayer = 0;
  List<Player> players = [];

  int selectedSkill = 0;
  List<Skill> skills = [];

  int rollLimit = 5;
  List<Roll> rolls = [];

  @override
  void initState() {
    Player.queryAll(widget.game.id)
      .then((value) {
        players = value;
        setState(() {});
      });

    Section.queryAll(widget.game.id)
        .then((value) {
      for(var section in value) {
        Skill.queryAll(section.id)
            .then((value) {
          for(var skill in value) {
            skills.add(skill);
          }
          setState(() {});
        });
      }
    });

    Roll.queryLast(5)
        .then((value) {
      for (var roll in value) {
        rolls.add(roll);
      }
      setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: const Color(0xFF061c37),
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                        children: [

                          // Result
                          Container(
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text('Résultat',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                            child: TextFormField(
                              decoration: const CustomInputDecoration(hint: 'Resultat'),
                              controller: resultController,
                              keyboardType: TextInputType.number,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Résultat manquant';
                                }
                                return null;
                              },
                            ),
                          ),

                          // Skill selection
                          Container(
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text('Compétence',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17
                              ),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(Radius.circular(5)),
                                border : Border.all()
                            ),
                            child: DropdownButtonFormField(
                              decoration: const InputDecoration.collapsed(hintText: 'Compétence',),
                              isExpanded: true,
                              dropdownColor: Colors.white,
                              padding: const EdgeInsets.all(20),
                              onChanged: (int? newValue){
                                setState(() {
                                  selectedSkill = newValue!;
                                });
                              },
                              items: skillList,
                              validator: (value) {
                                if (value == null || value == 0) {
                                  return 'Selectionnez une compétence';
                                }
                                return null;
                              },
                            ),
                          ),

                          // Player selection
                          Container(
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text('Joueur',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17
                              ),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(Radius.circular(5)),
                                border : Border.all()
                            ),
                            child: DropdownButtonFormField(
                              decoration: const InputDecoration.collapsed(hintText: 'Joueur'),
                              isExpanded: true,
                              dropdownColor: Colors.white,
                              padding: const EdgeInsets.all(20),
                              onChanged: (int? newValue){
                                setState(() {
                                  selectedPlayer = newValue!;
                                });
                              },
                              items: playersList,
                              validator: (value) {
                                if (value == null || value == 0) {
                                  return 'Selectionnez un joueur';
                                }
                                return null;
                              },
                            ),
                          ),

                          // Validate button
                          ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                Roll.insert(int.parse(resultController.text), selectedSkill, selectedPlayer)
                                    .then((value) {

                                  resultController.text = '';

                                  Fluttertoast.showToast(
                                      msg: "Résultat enregistré",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.green,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );

                                  Roll.queryLast(rollLimit)
                                      .then((value) {
                                    rolls = [];
                                    for (var roll in value) {
                                      rolls.add(roll);
                                    }
                                    setState(() {});
                                  });

                                });
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.green
                            ),
                            child: const Text('Ajouter',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ]

                    ),
                  ),
                ],
              ),
            )
          ),
        )
    );
  }

  //Create players list for Dropdown menu
  List<DropdownMenuItem<int>> get skillList {
    List<DropdownMenuItem<int>> menuItems = [];

    for (var skill in skills) {
      menuItems.add(DropdownMenuItem(value: skill.id, child: Text(skill.title)));
    }

    return menuItems;
  }

  //Create players list for Dropdown menu
  List<DropdownMenuItem<int>> get playersList {
    List<DropdownMenuItem<int>> menuItems = [];

    for (var player in players) {
      menuItems.add(DropdownMenuItem(
          value: player.id,
          child: Text('${player.title}${player.subtitle != '' ? ' (${player.subtitle})' : '' }')
      ));
    }

    return menuItems;
  }
}