import 'package:dice_stats/data/models/game.dart';
import 'package:dice_stats/presentation/ui/views/player_view.dart';
import 'package:dice_stats/presentation/ui/views/add_roll_view.dart';
import 'package:dice_stats/presentation/ui/views/last_roll_view.dart';
import 'package:dice_stats/presentation/ui/views/skill_view.dart';
import 'package:flutter/material.dart';

import 'package:dice_stats/data/database_helper.dart';

class GameView extends StatefulWidget {
  const GameView({super.key, required this.game});

  final Game game;

  @override
  State<GameView> createState() => _GameViewState();
}

class _GameViewState extends State<GameView> {

  final dbHelper = DatabaseHelper();

  int _selectedIndex = 0;
  static List<Widget> _pages = [];

  @override
  void initState() {
    super.initState();

    _pages = <Widget>[
      AddRollView(game: widget.game),
      LastRollView(game: widget.game),
      SkillView(game: widget.game),
      PlayerView(game: widget.game),

    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: const Color(0xFF061c37),
        appBar: AppBar(
          title: Text(widget.game.title),
        ),
        body: SafeArea(
          child: Center(
            child: _pages.elementAt(_selectedIndex),
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            const BottomNavigationBarItem(
              icon: Icon(Icons.add),
              label: 'Nouveau lancer',
            ),
            BottomNavigationBarItem(
              icon: Image.asset('assets/images/d20.png', width: 25, height: 25,),
              label: 'Derniers lancés',
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'Compétences',
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Joueurs',
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          selectedItemColor: Colors.blue.shade900,
          unselectedItemColor: Colors.black,
          showSelectedLabels: true,
          showUnselectedLabels: true,
        ),// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}