import 'package:dice_stats/data/database_helper.dart';

class Skill {

  Skill({required this.id, required this.title,  required this.subtitle, required this.sectionId});
  final int id;
  final String title;
  final String subtitle;
  final int sectionId;

  static Future<int> insert(String title, String subtitle, int sectionId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.insert('skill', {
      'title': title,
      'subtitle': subtitle,
      'section_id': sectionId
    });
  }

  static Future<Skill> query(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> skill = await dbHelper.query('skill', 'id', id, 0, 'id');

    return Skill(id: skill[0]['id'], title: skill[0]['title'], subtitle: skill[0]['subtitle'], sectionId: skill[0]['section_id']);
  }

  static Future<List<Skill>> queryAll(int sectionId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> skills = await dbHelper.query('skill', 'section_id', sectionId, 0, 'title');

    List<Skill> result = [];

    for(var skill in skills) {
      result.add(Skill(id: skill['id'], title: skill['title'], subtitle: skill['subtitle'], sectionId: skill['section_id']));
    }

    return result;
  }

  static Future<int> delete(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.delete('skill', id);
  }

  static Future<int> update(int id, String title, String subtitle, int sectionId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.update('skill', {
      'title': title,
      'subtitle': subtitle,
      'section_id': sectionId
    },  id);
  }

}