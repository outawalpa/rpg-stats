import 'package:dice_stats/data/database_helper.dart';

class Roll {

  Roll({required this.id, required this.result, required this.playerId, required this.skillId});
  final int id;
  final int result;
  final int playerId;
  final int skillId;

  static Future<int> insert(int result, int skillId, int playerId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.insert('roll', {
      'result': result,
      'skill_Id': skillId,
      'player_Id': playerId
    });
  }

  static Future<Roll> query(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> roll = await dbHelper.query('roll', 'id', id, 0, 'id');

    return Roll(id: roll[0]['id'], result: roll[0]['result'], skillId: roll[0]['skill_id'], playerId: roll[0]['player_id'] );
  }

  static Future<List<Roll>> queryAll(int skillId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> rolls = await dbHelper.query('roll', 'skill_id', skillId, 0, 'id');

    List<Roll> result = [];

    for(var roll in rolls) {
      result.add(Roll(id: roll['id'], result: roll['result'], skillId: roll['skill_id'], playerId: roll['player_id']));
    }

    return result;
  }

  static Future<List<Roll>> queryAllForPlayer(int skillId, int playerId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> rolls = await dbHelper.queryPlayerRolls(playerId, skillId);

    List<Roll> result = [];

    for(var roll in rolls) {
      result.add(Roll(id: roll['id'], result: roll['result'], skillId: roll['skill_id'], playerId: roll['player_id']));
    }

    return result;
  }

  static Future<List<Roll>> queryLast(int limit) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> rolls = await dbHelper.queryAllRows('roll', 'id');

    List<Map<String, dynamic>> reverseRolls = rolls.reversed.toList();

    List<Roll> result = [];

    if (limit != 0 && reverseRolls.length >= limit) {
      reverseRolls = reverseRolls.sublist(0, limit);
    }

    for(var roll in reverseRolls) {
      result.add(Roll(id: roll['id'], result: roll['result'], skillId: roll['skill_id'], playerId: roll['player_id']));
    }

    return result;
  }

  static Future<int> delete(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.delete('roll', id);
  }

}