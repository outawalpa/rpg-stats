import 'package:dice_stats/data/database_helper.dart';

class Section {

  Section({required this.id, required this.title, required this.gameId});
  final int id;
  final String title;
  final int gameId;

  static Future<int> insert(String title, int gameId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.insert('section', {
      'title': title,
      'game_id': gameId
    });
  }

  static Future<Section> query(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> section = await dbHelper.query('section', 'id', id, 0, 'id');

    return Section(id: section[0]['id'], title: section[0]['title'], gameId: section[0]['game_id']);
  }

  static Future<List<Section>> queryAll(int gameId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> sections = await dbHelper.query('section', 'game_id', gameId, 0, 'title');

    List<Section> result = [];

    for(var section in sections) {
      result.add(Section(id: section['id'], title: section['title'], gameId: section['game_id']));
    }

    return result;
  }

  static Future<int> delete(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.delete('section', id);
  }

  static Future<int> update(int id, String title, int gameId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.update('section', {
      'title': title,
      'game_id': gameId
    }, id);
  }

}