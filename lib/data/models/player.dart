import 'package:dice_stats/data/database_helper.dart';

class Player {

  Player({required this.id, required this.title, required this.subtitle,required this.color, required this.gameId});
  final int id;
  final String title;
  final String subtitle;
  final int color;
  final int gameId;

  static Future<int> insert(String title, String subtitle, int color, int gameId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.insert('player', {
      'title': title,
      'subtitle': subtitle,
      'color': color,
      'game_id': gameId
    });
  }

  static Future<Player> query(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> player = await dbHelper.query('player', 'id', id, 0, 'id');

    return Player(id: player[0]['id'], title: player[0]['name'], subtitle: player[0]['subtitle'], color: player[0]['color'], gameId: player[0]['game_id']);
  }

  static Future<List<Player>> queryAll(int gameId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> players = await dbHelper.query('player', 'game_id', gameId, 0, 'id');

    List<Player> result = [];

    for(var player in players) {
      result.add(Player(id: player['id'], title: player['title'], subtitle: player['subtitle'], color: player['color'], gameId: player['game_id']));
    }

    return result;
  }

  static Future<int> delete(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.delete('player', id);
  }

  static Future<int> update(int id, String title, String subtitle, int color, int gameId) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.update('player', {
      'title': title,
      'subtitle': subtitle,
      'color': color,
      'game_id': gameId
    }, id);
  }

}