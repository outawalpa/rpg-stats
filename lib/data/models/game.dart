import 'package:dice_stats/data/database_helper.dart';

class Game {

  Game({required this.id, required this.title, required this.subtitle});
  final int id;
  final String title;
  final String subtitle;

  static Future<int> insert(String title, String subtitle) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.insert('game', {
      'title': title,
      'subtitle': subtitle,
    });
  }

  static Future<Game> query(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> game = await dbHelper.query('game', 'id', id, 0, 'id');

    return Game(id: game[0]['id'], title: game[0]['title'], subtitle: game[0]['subtitle']);
  }

  static Future<List<Game>> queryAll() async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    List<Map<String, dynamic>> games = await dbHelper.queryAllRows('game', 'id');

    List<Game> result = [];

    for(var game in games) {
      result.add(Game(id: game['id'], title: game['title'], subtitle: game['subtitle']));
    }

    return result;
  }

  static Future<int> delete(int id) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.delete('game', id);
  }

  static Future<int> update(int id, String title, String subtitle) async {
    final dbHelper = DatabaseHelper();
    await dbHelper.init();

    return await dbHelper.update('game', {
      'title': title,
      'subtitle': subtitle,
    }, id);
  }

}