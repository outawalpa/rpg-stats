import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static const _databaseName = "MyDatabase.db";
  static const _databaseVersion = 1;

  late Database _db;

  // this opens the database (and creates it if it doesn't exist)
  Future<void> init() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, _databaseName);
    _db = await openDatabase(
      path,
      version: _databaseVersion,
      onCreate: _onCreate,
    );
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE game (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            subtitle TEXT NOT NULL
          )
          ''');

    await db.execute('''
          CREATE TABLE player (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            subtitle TEXT NOT NULL,
            color INTEGER NOT NULL,
            game_id NOT NULL,
            FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE ON UPDATE NO ACTION
          )
          ''');

    await db.execute('''
          CREATE TABLE section (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            game_id INTEGER NOT NULL
          )
          ''');

    await db.execute('''
          CREATE TABLE skill (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            subtitle TEXT NOT NULL,
            section_id INTEGER NOT NULL,
            FOREIGN KEY (section_id) REFERENCES section (id) ON DELETE CASCADE ON UPDATE NO ACTION
          )
          ''');

    await db.execute('''
          CREATE TABLE roll (
            id INTEGER PRIMARY KEY,
            result INTEGER NOT NULL,
            player_id INTEGER NOT NULL,
            skill_id INTEGER NOT NULL,
            FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE ON UPDATE NO ACTION
            FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE ON UPDATE NO ACTION
          )
          ''');
  }

  Future<int> insert(String table, Map<String, dynamic> row) async {
    return await _db.insert(table, row);
  }

  Future<int> delete(String table, int id) async {
    return await _db.delete(
      table,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<int> update(String table, Map<String, dynamic> values, int id) async {
    return await _db.update(
        table,
        values,
        where: 'id = ?',
        whereArgs: [id]
    );
  }

  Future<List<Map<String, dynamic>>> query(String table, String where, dynamic value, int limit, String orderBy) async {

    if(limit == 0) {
      return await _db.query(
          table,
          where: '$where = ?',
          whereArgs: [value],
          orderBy: orderBy
      );
    } else {
      return await _db.query(
          table,
          where: '$where = ?',
          whereArgs: [value],
          limit: limit,
          orderBy: orderBy
      );
    }

  }

  Future<List<Map<String, dynamic>>> queryAllRows(String table, String? orderBy) async {
    return await _db.query(
        table,
        orderBy: orderBy
    );
  }

  Future<List<Map<String, dynamic>>> queryPlayerRolls(int playerId, int skillId) async {
    return await _db.rawQuery(
      'SELECT * FROM roll WHERE player_id=? and skill_id=?',
      [playerId, skillId]
    );
  }

}